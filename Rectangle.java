public class Rectangle implements Figure {
    private int sideA;

    private int sideB;

    private int surfaceArea;

    private int perimeter;

    public Rectangle(int sideA, int sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }

    public int getSurfaceArea() {
        return surfaceArea;
    }

    public int getPerimeter() {
        return perimeter;
    }

    @Override
    public void countPerimeter() {
        this.perimeter = 2 * this.sideA + 2 * this.sideB;
    }

    @Override
    public void countsurfaceArea() {
        this.surfaceArea = this.sideA * this.sideB;
    }
}
